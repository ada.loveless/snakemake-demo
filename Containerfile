FROM fedora

# 1. Common sense tools
# 2. More common sense
# 3. For snakemake demo
# 4. For some reason trimgalore wants this?
RUN dnf install -y neovim zsh git stow \
    gcc tree rsync corkscrew \
    environment-modules apptainer \
    libxcrypt-compat

# Install zsh and nvim defaults
WORKDIR "/root"
RUN git clone https://gitlab.com/ada.loveless/dotfiles .dotfiles
WORKDIR "/root/.dotfiles"
RUN set -eux; \
    stow nvim; \
    stow zsh
WORKDIR "/root"
RUN set -eux; \
    git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim; \
    nvim --headless -u ~/.config/nvim/lua/plugins.lua -c 'autocmd User PackerComplete quitall' -c 'PackerSync'

ENV ZELLIJ_SOURCE "https://github.com/zellij-org/zellij/releases/download/v0.37.2/zellij-x86_64-unknown-linux-musl.tar.gz"
ENV ZELLIJ_SHA256SUM "0d3bc9e261d093ebde1876c3fd38ec392561487c9b5fc820e38e2fde4d28a62f"
# Install zellij
RUN set -eux; \
    curl --proto '=https' --tlsv1.2 -LsSf "${ZELLIJ_SOURCE}" --output zellij.tar.gz; \
    mkdir -p .local/bin; \
    tar xf zellij.tar.gz -C ~/.local/bin; \
    rm zellij.tar.gz; \
    echo "${ZELLIJ_SHA256SUM}  .local/bin/zellij" | sha256sum -c

# Install mamba
ENV MAMBA_SOURCE "https://github.com/conda-forge/miniforge/releases/download/23.1.0-3/Mambaforge-23.1.0-3-Linux-x86_64.sh"
ENV MAMBA_SHA256SUM "7a6a07de6063245163a87972fb15be3a226045166eb7ee526344f82da1f3b694"
RUN set -eux; \
    curl -LOsSf "${MAMBA_SOURCE}"; \
    echo "${MAMBA_SHA256SUM}  Mambaforge-23.1.0-3-Linux-x86_64.sh" | sha256sum -c; \
    bash "Mambaforge-23.1.0-3-Linux-x86_64.sh" -p /opt/mambaforge -b; \
    rm "Mambaforge-23.1.0-3-Linux-x86_64.sh"; \
    /opt/mambaforge/bin/mamba init; \
    /opt/mambaforge/bin/mamba init zsh
ENV PATH /opt/mambaforge/bin:${PATH}

# Install ffq
RUN pip install ffq

# Install snakemake
RUN set -eux; \
    mamba install -y -c bioconda snakemake pandas
VOLUME ["/data"]
WORKDIR "/data"
ENTRYPOINT ["/bin/zsh"]
CMD []
