import pandas as pd

data = pd.read_csv("config/sra.txt")
data = data.loc[data["Assay Type"] == "RNA-Seq",:]
data = data.sort_values("Bases")[0:3]
IDS = data.Run

GENOME_URI = "https://ftp.ensembl.org/pub/release-109/fasta/mus_musculus/dna/Mus_musculus.GRCm39.dna_sm.primary_assembly.fa.gz"
GENOME_FA = "genome/Mus_musculus.GRCm39.dna_sm.primary_assembly.fa.gz"
REFERENCE_BOWTIE = "genome/bowtie2"

rule all:
    input:
        expand("20_map/{id}.bam", id=IDS)

rule download:
    output:
        "00_fastq/{id}.fastq.gz"
    shell:
        """
        ffq --ftp {wildcards.id} | grep -Eo '"url": "[^"]*"' | grep -o '"[^"]*"$' | xargs curl -o {output}
        """

rule shorten:
    input:
        fq="00_fastq/{id}.fastq.gz"
    output:
        fq="05_short/{id}.fastq.gz"
    shell:
        """
        set +o pipefail
        zcat {input.fq} | head -4000 | gzip -c > {output.fq}
        """

rule trim:
    input:
        r1="05_short/{id}.fastq.gz"
    output:
        r1="10_trim/{id}_trimmed.fq.gz"
    conda:
        "envs/trimgalore.yml"
    shell:
        "trim_galore "
        "-o 10_trim --basename {wildcards.id} "
        "{input.r1} "

rule download_reference:
    output:
        GENOME_FA
    shell:
        "curl -o {output} {GENOME_URI}"

rule bowtie_index:
    input:
        fa=GENOME_FA
    output:
        directory(REFERENCE_BOWTIE)
    conda:
        "envs/bowtie.yml"
    threads: 4
    shell:
        "bowtie2-build {input.fa} genome/bowtie2"

rule map:
    input:
        r1="10_trim/{id}_trimmed.fq.gz",
        ref=REFERENCE_BOWTIE
    output:
        bam="20_map/{id}.bam",
    conda:
        "envs/bowtie.yml"
    threads: 4
    shell:
        "bowtie2 "
        "--very-sensitive-local "
        "-p {threads} -x {input.ref} "
        "{input.r1} | samtools view -b > {output.bam}"
